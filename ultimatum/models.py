from otree.api import (
    models, BaseConstants, BaseSubsession, BaseGroup, BasePlayer,
    Currency as c, currency_range,
)
import random
import psycopg2
from psycopg2.extensions import AsIs
from ultimatum_config import CONFIG

################################ database creation methods #######################################

username = CONFIG['db_username']
password = CONFIG['db_password']
schema_name = CONFIG['db_schema_name']
address = CONFIG['db_address']
database_name = CONFIG['db_name']


def create_database():
    conn = psycopg2.connect(host=address, database=database_name, user=username, password=password)
    cur = conn.cursor()
    command = """ CREATE SCHEMA IF NOT EXISTS %s AUTHORIZATION %s ;"""
    params = (AsIs(schema_name), AsIs(username),)

    try:
        cur.execute(command, params)
        cur.close()
        conn.commit()
    except Exception as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def create_table_all_data():
    conn = psycopg2.connect(host=address, database=database_name, user=username, password=password)
    cur = conn.cursor()
    command = """ CREATE TABLE IF NOT EXISTS %s.ALL_DATA (
                                                SESSION_CODE VARCHAR(30) NOT NULL,
                                                ROUND INTEGER NOT NULL,
                                                PLAYER_ID INTEGER NOT NULL,
                                                COMPLEX_ONLINE VARCHAR(1) NOT NULL,
                                                MESSAGE VARCHAR(100) NOT NULL,
                                                AMOUNT_OFFERED INTEGER NOT NULL,
                                                OFFER_RATING INTEGER NOT NULL,
                                                TIME_STAMP TIMESTAMPTZ NOT NULL,
                                                PRIMARY KEY (SESSION_CODE,ROUND,PLAYER_ID)
                                                ); """
    param = (AsIs(schema_name),)

    try:
        cur.execute(command, param)
        cur.close()
        conn.commit()
    except Exception as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


##################################################################################################

class Constants(BaseConstants):
    name_in_url = 'ultimatum'

    # only one player is in a group
    players_per_group = None

    # setting round number to a high number so it could be controlled dynamicly in the app, more info: https://otree.readthedocs.io/en/latest/rounds.html
    num_rounds = 100

    instructions_template = '../_templates/global/Instructions.html'
    popup_template = 'ultimatum/PopupMessage.html'

    #  maximal amount that can be offered to a player
    endowment = c(100)
    payoff_if_rejected = c(0)
    offer_increment = c(1)

    messages = CONFIG['messages']

    #  range of all the offers a player can get
    offer_choices = currency_range(0, endowment, offer_increment)
    offer_choices_count = len(offer_choices)


class Subsession(BaseSubsession):

    # if the app has multiple rounds, creating_session gets run multiple times consecutively
    def creating_session(self):
        create_database()
        create_table_all_data()
        for p in self.get_players():
            p.amount_offered = random.choice(Constants.offer_choices)
            p.complex_mode_online = self.session.config['complex_mode_online']
            p.playedOffline = (self.session.__dict__['config']['name'] == "ultimatum_strategy_before_game")
            p.set_message()


class Group(BaseGroup):
    pass


class Player(BasePlayer):
    # the player's total payoff in all rounds
    payoff = models.CurrencyField(initial=0)

    # the amount that is offered for each player in every round
    amount_offered = models.CurrencyField(initial=0)

    total_amount_offered = models.CurrencyField(initial=0)

    # the total number of points the proposer and the divider had to divide between themselves.
    # usually, current_endowment = round_number * endowment
    current_endowment = models.CurrencyField(initial=0)

    # the message displayed to the player in 'complex' mode
    message = models.StringField(initial='')

    # indicates the rating for the offer:{-1,0,1}
    offer_rating = models.IntegerField()

    # indicates if the 'complex_online' mode is on or not
    complex_mode_online = models.BooleanField(initial=False)

    # the amount the player earned in the offline game if played "strategy before game"
    threshold_payoff = models.CurrencyField(initial=0)

    # indicates if the player is playing this online game after an offline session
    playedOffline = models.BooleanField(initial=False)



    def set_payoff(self):
        if self.offer_rating == 1:  # ranked most acceptable
            if self.round_number > 1:
                self.payoff = self.amount_offered + self.in_round(self.round_number - 1).payoff
            else:
                self.payoff = self.amount_offered
        elif self.offer_rating == 0:  # ranked medium acceptability
            if self.round_number > 1:
                self.payoff = (self.amount_offered*0.5) + self.in_round(self.round_number - 1).payoff
            else:
                self.payoff = self.amount_offered*0.5
        else:  # ranked least acceptable
            if self.round_number > 1:
                self.payoff = Constants.payoff_if_rejected + self.in_round(self.round_number - 1).payoff
            else:
                self.payoff = Constants.payoff_if_rejected

    def set_total_amount_offered(self):
        if self.round_number > 1:
            self.total_amount_offered = self.amount_offered + self.in_round(self.round_number - 1).total_amount_offered
        else:
            self.total_amount_offered = self.amount_offered
        self.participant.vars["GamePayoff"] += self.total_amount_offered

    def set_message(self):
        if self.complex_mode_online:
            message_type_index = self.session.config["message_type"] - 1
            self.message = Constants.messages[message_type_index][(self.round_number - 1) % 3]

