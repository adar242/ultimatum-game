from ._builtin import Page
import psycopg2
from psycopg2.extensions import AsIs
import datetime
from ultimatum_config import CONFIG
from .models import Constants
import random

username = CONFIG['db_username']
password = CONFIG['db_password']
schema_name = CONFIG['db_schema_name']
address = CONFIG['db_address']
database_name = CONFIG['db_name']


########################## utility functions #############################

def store_players_data(session_code, round_num, player_id,
                       is_complex_online, amount_offered, message, offer_rating, time_stamp):
    conn = psycopg2.connect(host=address, database=database_name, user=username, password=password)
    cur = conn.cursor()

    insert = """INSERT INTO %s.ALL_DATA(
                                  SESSION_CODE, ROUND, PLAYER_ID, COMPLEX_ONLINE, AMOUNT_OFFERED, MESSAGE, OFFER_RATING, TIME_STAMP)
                                  VALUES (%s, %s, %s, %s, %s, %s, %s, %s);"""
    params = (
        AsIs(schema_name), session_code, AsIs(round_num), AsIs(player_id), is_complex_online, AsIs(amount_offered), message,
        offer_rating, time_stamp,)

    try:
        cur.execute(insert, params)
        cur.close()
        conn.commit()

    except Exception as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def currency_to_int(currency_field):
    #  removes the ' points' suffix from the currency's string representation and converts it to int
    return int(str(currency_field)[:-7])


#########################################################################


class Introduction(Page):
    def is_displayed(self):
        if self.player.playedOffline:
            return False
        else:
            return self.round_number == 1



class GameAfterIntro(Page):
    def is_displayed(self):
        if self.player.playedOffline:
            return self.round_number == 1
        else:
            return False


class WaitingPage(Page):
    def is_displayed(self):
        if not self.player.playedOffline:
            return self.round_number == 1
        else:
            return False


class OnlineTaskInstructions(Page):
    def is_displayed(self):
        if self.player.playedOffline:
            return False
        else:
            return self.round_number == 1


class Accept(Page):
    form_model = 'player'
    form_fields = ['offer_rating']

    def is_displayed(self):
        if self.player.complex_mode_online:
            return self.round_number <= self.session.config["num_of_tasks_online"] * 3  # multiplying by 3 because in complex mode a round is defined by going through all 3 types of messages

        return self.round_number <= self.session.config["num_of_tasks_online"]

    def before_next_page(self):
        if self.round_number == 1:  # if this is the first round, init the participant's var ""GamePayoff"" with 0
            self.participant.vars["GamePayoff"] = 0



        # each session has a unique code which is stored in the session's dictionary
        session_code = self.session.__dict__['code']

        round_num = self.round_number
        player_id = self.player.id_in_group

        if self.player.complex_mode_online:
            is_complex_online = 'Y'
        else:
            is_complex_online = 'N'

        amount_offered = currency_to_int(self.player.amount_offered)

        self.player.set_total_amount_offered()

        self.player.current_endowment = round_num * Constants.endowment

        message = self.player.message

        offer_rating = self.player.offer_rating

        time_stamp = str(datetime.datetime.now())

        store_players_data(session_code, round_num, player_id, is_complex_online, amount_offered,
                           message, offer_rating, time_stamp)

        self.player.set_payoff()




class Results(Page):
    def is_displayed(self):
        if self.player.complex_mode_online:
            return self.round_number <= self.session.config["num_of_tasks_online"] * 3  # multiplying by 3 to go over all types of messages
        else:
            return self.round_number <= self.session.config["num_of_tasks_online"]

    def before_next_page(self):
        if self.player.playedOffline:  # if playing after offline retrieving the payoff from offline game
            self.player.threshold_payoff = self.participant.vars["ThresholdPayoff"]


class FinalResults(Page):
    def is_displayed(self):
        if self.session.__dict__['config']['name'] == "ultimatum_game_before_strategy":
            return False
        else:
            if self.player.complex_mode_online:
                return self.round_number == self.session.config["num_of_tasks_online"] * 3  # multiplying by 3 to go over all types of messages
            else:
                return self.round_number == self.session.config["num_of_tasks_online"]



page_sequence = [Introduction,
                 GameAfterIntro,
                 WaitingPage,
                 OnlineTaskInstructions,
                 Accept,
                 Results,
                 FinalResults]
