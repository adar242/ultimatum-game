from otree.api import (
    models, BaseConstants, BaseSubsession, BaseGroup, BasePlayer, Currency as c, currency_range
)
import psycopg2
from psycopg2.extensions import AsIs

from ultimatum_config import CONFIG

author = 'Roy Zerbib'

messages = CONFIG['messages']

################################ database creation methods #######################################

username = CONFIG['db_username']
password = CONFIG['db_password']
address = CONFIG['db_address']
schema_name = CONFIG['db_schema_name']
database_name = CONFIG['db_name']


def create_database():
    conn = psycopg2.connect(host=address, database=database_name, user=username, password=password)
    cur = conn.cursor()
    command = """ CREATE SCHEMA IF NOT EXISTS %s AUTHORIZATION %s ;"""
    params = (AsIs(schema_name), AsIs(username),)

    try:
        cur.execute(command, params)
        cur.close()
        conn.commit()
    except Exception as error:
        print(str(error))
    finally:
        if conn is not None:
            conn.close()


def create_table_thresholds():
    conn = psycopg2.connect(host=address, database=database_name, user=username, password=password)
    cur = conn.cursor()
    command = """ CREATE TABLE IF NOT EXISTS %s.THRESHOLDS (
                                            SESSION_CODE VARCHAR(30) NOT NULL,
                                            ROUND INTEGER NOT NULL,
                                            PLAYER_ID INTEGER NOT NULL,
                                            MESSAGE VARCHAR(100) NOT NULL,
                                            MIN_ACCEPT INTEGER NOT NULL,
                                            MAX_REJECT INTEGER NOT NULL,
                                            COMPLEX_OFFLINE VARCHAR(1) NOT NULL,
                                            TIME_STAMP TIMESTAMPTZ NOT NULL,
                                            PRIMARY KEY (SESSION_CODE,ROUND, PLAYER_ID, MESSAGE)
                                            ); """
    param = (AsIs(schema_name),)

    try:
        cur.execute(command, param)
        cur.close()
        conn.commit()
    except Exception as error:
        print(str(error))
    finally:
        if conn is not None:
            conn.close()


##################################################################################################


class Constants(BaseConstants):
    name_in_url = 'choose_thresholds'
    players_per_group = None

    instructions_template = '../_templates/global/Instructions.html'

    # setting round number to a high number so it could be controlled dynamicly in the app, more info: https://otree.readthedocs.io/en/latest/rounds.html
    num_rounds = 20

    endowment = c(100)
    payoff_if_rejected = c(0)
    offer_increment = c(1)

    offer_choices = currency_range(0, endowment, offer_increment)
    offer_choices_count = len(offer_choices)


class Subsession(BaseSubsession):

    # if the app has multiple rounds, creating_session gets run multiple times consecutively
    def creating_session(self):
        create_database()
        create_table_thresholds()
        for p in self.get_players():
            p.complex_mode_offline = self.session.config['complex_mode_offline']
            p.playedOnline = (self.session.__dict__['config']['name'] == "ultimatum_game_before_strategy")


class Group(BaseGroup):
    pass


class Player(BasePlayer):
    # the minimal amount the player would accept for selfish offer
    min_accept_first = models.IntegerField(initial=0)

    # the maximal amount the player would reject for selfish offer
    max_reject_first = models.IntegerField(initial=0)

    # the minimal amount the player would accept for generous offer
    min_accept_second = models.IntegerField(initial=0)

    # the maximal amount the player would reject for generous offer
    max_reject_second = models.IntegerField(initial=0)

    # the minimal amount the player would accept for average offer
    min_accept_third = models.IntegerField(initial=0)

    # the maximal amount the player would reject for average offer
    max_reject_third = models.IntegerField(initial=0)

    # the maximal amount the player would reject for offers when not in complex mode
    max_reject_uncomplex = models.IntegerField(initial=0)

    # the minimal amount the player would accept for offers when not in complex mode
    min_accept_uncomplex = models.IntegerField(initial=0)

    # the number of message that is displayed to the player
    index = models.IntegerField(initial=0)

    # the message displayed to the player in 'complex' mode
    message = models.StringField(initial='')

    # the total amount the player earned in this part of the game
    amount_earned = models.CurrencyField(initial=0)

    # the amount the player earned in the online game if played "game before strategy"
    game_payoff = models.CurrencyField(initial=0)

    # indicates if the 'complex_online' mode is on or not
    complex_mode_offline = models.BooleanField(initial=False)

    # indicates if the player is playing this online game after an offline session
    playedOnline = models.BooleanField(initial=False)

    def set_message(self):
        # setting the next message that will be displayed to the player in 'complex_mode'.
        message_type_index = self.session.config['message_type'] - 1

        self.message = messages[message_type_index][self.index]
        if (self.index != 2):
            self.index = self.index + 1
        else:
            self.index = 0
