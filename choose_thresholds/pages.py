import datetime

import psycopg2
from psycopg2.extensions import AsIs
from ultimatum_config import CONFIG
from ultimatum._builtin import Page
import random

username = CONFIG['db_username']
password = CONFIG['db_password']
address = CONFIG['db_address']
schema_name = CONFIG['db_schema_name']
database_name = CONFIG['db_name']
messages = CONFIG['messages']
offers_value = CONFIG['offers_value']
offers_type = CONFIG['offers_type']




########################## utility functions #############################

def store_players_thresholds(session_code, round_num, player_id, message, min_accept, max_reject, complex_offline, time_stamp):
    conn = psycopg2.connect(host=address, database=database_name, user=username, password=password)
    cur = conn.cursor()

    insert = """INSERT INTO %s.THRESHOLDS(
                                     SESSION_CODE, ROUND, PLAYER_ID, MESSAGE, MIN_ACCEPT, MAX_REJECT,COMPLEX_OFFLINE, TIME_STAMP)
                                     VALUES (%s, %s, %s, %s, %s, %s, %s, %s );"""
    params = (
        AsIs(schema_name), session_code, AsIs(round_num),AsIs(player_id), message, AsIs(min_accept), AsIs(max_reject), complex_offline, time_stamp)

    try:
        cur.execute(insert, params)
        cur.close()
        conn.commit()
    except Exception as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def currency_to_int(currency_field):
    #  removes the ' points' suffix from the currency's string representation and converts it to int
    if (str(currency_field)[:-7]) == "":
        return 1

    return int(str(currency_field)[:-7])


#########################################################################


class Introduction(Page):
    def is_displayed(self):
        if self.player.playedOnline:
            return False
        else:
            return self.round_number == 1


class ThresholdsAfterIntro(Page):
    def is_displayed(self):
        if self.player.playedOnline:
            return True
        else:
            return self.round_number == 1



class WaitingPage(Page):
    def is_displayed(self):
        if self.player.playedOnline:
            return False
        else:
            return self.round_number == 1


class OfflineTaskInstructions(Page):
    def is_displayed(self):
        if self.player.playedOnline:
            return False
        else:
            return self.round_number == 1


class OfflineResults(Page):
    def is_displayed(self):
        return self.round_number <= self.session.config["num_of_tasks_offline"]

    def before_next_page(self):
        if self.player.playedOnline: # retrieving the payoff from the online game
            self.player.game_payoff = self.participant.vars["GamePayoff"]


# this page that contain a single slider is shown when not complex_offline
class UncomplexThreshold(Page):
    def is_displayed(self):
        if self.session.config['complex_mode_offline']:
            return False
        else:
            return self.round_number <= self.session.config["num_of_tasks_offline"]

    form_model = 'player'
    form_fields = ['min_accept_uncomplex', 'max_reject_uncomplex']

    def before_next_page(self):
        session_code = self.session.__dict__['code']
        player_id = self.participant.id_in_session
        round_num = self.round_number

        #store threshold for the slider
        if self.player.complex_mode_offline:
            is_complex_offline = 'Y'
        else:
            is_complex_offline = 'N'
        message = "not in complex mode for offline game"
        min_accept_uncomplex = self.player.min_accept_uncomplex
        max_reject_uncomplex = self.player.max_reject_uncomplex
        time_stamp = str(datetime.datetime.now())

        store_players_thresholds(session_code,round_num, player_id, message, min_accept_uncomplex, max_reject_uncomplex,is_complex_offline, time_stamp)

        if self.round_number == 1:  # if this is the first round, init the participant's var "thresholdPayoff" with 0
            self.participant.vars["ThresholdPayoff"] = 0
        else:  # not first round, updating amount earned with payoff from previous rounds
            self.player.amount_earned = self.participant.vars["ThresholdPayoff"]

        #updating amount_earned for each of the offers
        for i in range(20):
            if offers_value[i] >= min_accept_uncomplex: # the offer's value is in the player acceptance zone,so he get the whole offer
                self.player.amount_earned = self.player.amount_earned + offers_value[i]
            elif offers_value[i] > max_reject_uncomplex: # the pffer's value is in the player's indifference zone, so he get half the offer
                self.player.amount_earned = self.player.amount_earned + (offers_value[i] * 0.5)

        self.participant.vars["ThresholdPayoff"] += self.player.amount_earned  # adding the threshold payoff with the amount earned from this round


class ChooseThresholds(Page):
    def is_displayed(self):
        if not self.session.config['complex_mode_offline']:
            return False
        else:
            return self.round_number <= self.session.config["num_of_tasks_offline"]

    form_model = 'player'
    form_fields = ['min_accept_first', 'max_reject_first',
                   'min_accept_second', 'max_reject_second',
                   'min_accept_third', 'max_reject_third']


    def before_next_page(self):
        session_code = self.session.__dict__['code']
        player_id = self.participant.id_in_session;
        message_type_index = self.session.config['message_type'] - 1
        round_num = self.round_number

        if self.player.complex_mode_offline:
            is_complex_offline = 'Y'
        else:
            is_complex_offline = 'N'

        #store threshold for first slider (the "selfish" prediction)
        message = messages[message_type_index][0]
        min_accept = self.player.min_accept_first
        max_reject = self.player.max_reject_first
        time_stamp = str(datetime.datetime.now())


        store_players_thresholds(session_code, round_num, player_id, message, min_accept, max_reject,is_complex_offline, time_stamp)

        if self.round_number == 1:  # if this is the first round, init the participant's var "thresholdPayoff" with 0
            self.participant.vars["ThresholdPayoff"] = 0
        else:  # not first round, updating amount earned with payoff from previous rounds
            self.player.amount_earned = self.participant.vars["ThresholdPayoff"]

        #updating amount_earned for each of the offers with type=-1 (selfish offers)
        for i in range(20):
            if offers_type[i] == -1:  #this is an offer that is linked to the "selfish" range
                if offers_value[i] >= min_accept: #the offer's value is in the player acceptance zone,so he get the whole offer
                    self.player.amount_earned = self.player.amount_earned + offers_value[i]
                elif offers_value[i] > max_reject: #the pffer's value is in the player's indifference zone, so he get half the offer
                    self.player.amount_earned = self.player.amount_earned + (offers_value[i] * 0.5)



        #store threshold for second slider (the "generous" prediction
        message = messages[message_type_index][1]
        min_accept = self.player.min_accept_second
        max_reject = self.player.max_reject_second

        store_players_thresholds(session_code, round_num, player_id, message, min_accept, max_reject,is_complex_offline,time_stamp)

        # updating amount_earned for each of the offers with type=1 (generous offers)
        for i in range(20):
            if offers_type[i] == 1:  # this is an offer that is linked to the "generous" range
                if offers_value[i] >= min_accept:  # the offer's value is in the player acceptance zone,so he get the whole offer
                    self.player.amount_earned = self.player.amount_earned + offers_value[i]
                elif offers_value[i] > max_reject:  # the offer's value is in the player's indifference zone, so he get half the offer
                    self.player.amount_earned = self.player.amount_earned + (offers_value[i] * 0.5)

        #store threshold for third slider (the "average" prediction)
        message = messages[message_type_index][2]
        min_accept = self.player.min_accept_third
        max_reject = self.player.max_reject_third

        store_players_thresholds(session_code, round_num, player_id, message, min_accept, max_reject,is_complex_offline, time_stamp)

        # updating amount_earned for each of the offers with type=0 (average offers)
        for i in range(20):
            if offers_type[i] == 0:  # this is an offer that is linked to the "average" range
                if offers_value[i] >= min_accept :  # the offer's value is in the player acceptance zone,so he get the whole offer
                    self.player.amount_earned = self.player.amount_earned + offers_value[i]
                elif offers_value[i] > max_reject:  # the pffer's value is in the player's indifference zone, so he get half the offer
                    self.player.amount_earned = self.player.amount_earned + (offers_value[i] * 0.5)

        self.participant.vars["ThresholdPayoff"] += self.player.amount_earned  # adding the threshold payoff with the amount earned from this round


class WaitingforCounterpart(Page):
    def is_displayed(self):
        return self.round_number <= self.session.config["num_of_tasks_offline"]

    timeout_seconds = timeout_seconds = random.choice(range(12, 19, 1))

class FinalResults(Page):
    def is_displayed(self):
        if self.session.__dict__['config']['name'] == "ultimatum_strategy_before_game":
            return False
        else:
            return self.round_number == self.session.config["num_of_tasks_offline"]



page_sequence = [
    Introduction,
    ThresholdsAfterIntro,
    WaitingPage,
    OfflineTaskInstructions,
    UncomplexThreshold,
    ChooseThresholds,
    WaitingforCounterpart,
    OfflineResults,
    FinalResults
]
