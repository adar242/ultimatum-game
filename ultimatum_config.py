CONFIG = {
    "num_rounds": 3,
    # database parameters
    "db_address": "localhost",
    "db_name": "ultimatum",
    "db_username": "postgres",
    "db_password": "Gr43Zt",
    "db_schema_name": "users_data",
    "messages":
        [
            ["we indicate that you will probably get a very selfish offer",
             "we indicate that you will probably get a very generous offer",
             "we indicate that you will probably get an average offer"],
            ["type2 selfish",
             "type2 generous",
             "type2 average"],
            ["type3 selfish",
             "type3 generous",
             "type3 average"]
        ],
    # admin's username and password:
    "admin_username": "admin",
    "admin_password": "1234",
    # offers values and type to use in game:
    "offers_value": [1, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50,
                     60, 65, 70, 75, 80, 85, 90, 95, 100],
    "offers_type": [-1, -1, -1, -1, -1, -1, -1, 0, 0, 0,
                    0, 0, 0, 1, 1, 1, 1, 1, 1, 1]
}
